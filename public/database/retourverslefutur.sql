-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 12, 2021 at 11:20 AM
-- Server version: 10.4.18-MariaDB
-- PHP Version: 8.0.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `retourverslefutur`
--

-- --------------------------------------------------------

--
-- Table structure for table `article`
--

CREATE TABLE `article` (
  `id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  `author_id` int(11) NOT NULL,
  `picture` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `article`
--

INSERT INTO `article` (`id`, `title`, `content`, `created_at`, `author_id`, `picture`) VALUES
(7, 'Better Safe than Sorry', 'So what do you need to do before zombies…or hurricanes or pandemics for example, actually happen? First of all, you should have an emergency kit in your house. This includes things like water, food, and other supplies to get you through the first couple of days before you can locate a zombie-free refugee camp (or in the event of a natural disaster, it will buy you some time until you are able to make your way to an evacuation shelter or utility lines are restored). Below are a few items you should include in your kit, for a full list visit the CDC Emergency page.\r\n\r\nWater (1 gallon per person per day)\r\nFood (stock up on non-perishable items that you eat regularly)\r\nMedications (this includes prescription and non-prescription meds)\r\nTools and Supplies (utility knife, duct tape, battery powered radio, etc.)\r\nSanitation and Hygiene (household bleach, soap, towels, etc.)\r\nClothing and Bedding (a change of clothes for each family member and blankets)\r\nImportant documents (copies of your driver’s license, passport, and birth certificate to name a few)\r\nFirst Aid supplies (although you’re a goner if a zombie bites you, you can use these supplies to treat basic cuts and lacerations that you might get during a tornado or hurricane)\r\nOnce you’ve made your emergency kit, you should sit down with your family and come up with an emergency plan. This includes where you would go and who you would call if zombies started appearing outside your door step. You can also implement this plan if there is a flood, earthquake, or other emergency.\r\n\r\nPicture of Family by mailbox\r\nFamily members meeting by their mailbox. You should pick two meeting places, one close to your home and farther away\r\nIdentify the types of emergencies that are possible in your area. Besides a zombie apocalypse, this may include floods, tornadoes, or earthquakes. If you are unsure contact your local Red Cross chapter for more information.\r\nPick a meeting place for your family to regroup in case zombies invade your home…or your town evacuates because of a hurricane. Pick one place right outside your home for sudden emergencies and one place outside of your neighborhood in case you are unable to return home right away.\r\nIdentify your emergency contacts. Make a list of local contacts like the police, fire department, and your local zombie response team. Also identify an out-of-state contact that you can call during an emergency to let the rest of your family know you are ok.\r\nPlan your evacuation route. When zombies are hungry they won’t stop until they get food (i.e., brains), which means you need to get out of town fast! Plan where you would go and multiple routes you would take ahead of time so that the flesh eaters don’t have a chance! This is also helpful when natural disasters strike and you have to take shelter fast.', '2016-01-01 00:00:00', 12, 'ab56343b44c249af9d22b720560a9c34.jpg'),
(8, 'WHAT EXACTLY IS THE APOCALYPSE?', 'The idea of the end of humanity is something we’ve been flirting with for ages, and it’s as old as the beginning of mankind itself.\r\n\r\nThe ancient Mayans predicted it to happen in 2012 (well that didn’t happen as expected), the Terminator movie franchise called it Judgment Day, and Christians call it Rapture.\r\n\r\nRegardless of the name, majority of life on earth is believed to have a clock that’s always counting down to the Apocalypse.\r\n\r\nRegardless of how the apocalypse is to happen, you and I don’t intend to be caught unaware or be among the casualties either before, during, or after such apocalyptic event.\r\n\r\nWhen disease takes place, people often panic and get killed because they didn’t take any safety action immediately.\r\n\r\n“For safety is not a gadget but a state of mind.” Eleanor Everet\r\n\r\nIf you sit and think about it for a minute, you’ll realize that there are a million ways the apocalypse could happen, many of which you wouldn’t even see coming before it strikes.\r\n\r\nThat’s why you need to get yourself prepared for when any of the apocalypses strike, even though it only has a tiny percentage chance that it will happen.\r\n\r\nTrust me, we are incredibly fortunate we don’t live in a post-apocalyptic era, but that does not mean there is no need to prepare for any large scale event that could significantly impact lives on the planet.\r\n\r\nIt could happen right this moment that you’re reading this article or when you wake up from bed tomorrow!\r\n\r\nA survey shows that nearly 25% of British people have a plan to survive the zombie apocalypse. Paranoid or smart? You’ll find out soon enough.\r\n\r\nSo fasten your seat belts and join me on a journey as we explore this complete beginner’s guide to preparing for an apocalypse, surviving an apocalypse and the aftermath of an apocalyptic event.', '2016-01-01 00:00:00', 12, '7e36ac0cd8c3001db34b7875839d00af.jpg'),
(10, 'How did the Mayans knew about the end of the world?', 'There is a strong tradition of \"world ages\" in Maya literature, but the record has been distorted, leaving several possibilities open to interpretation. According to the Popol Vuh, a compilation of the creation accounts of the Kʼicheʼ Maya of the Colonial-era highlands, we are living in the fourth world. The Popol Vuh describes the gods first creating three failed worlds, followed by a successful fourth world in which humanity was placed. In the Maya Long Count, the previous world ended after 13 bʼakʼtuns, or roughly 5,125 years.The Long Count\'s \"zero date\" was set at a point in the past marking the end of the third world and the beginning of the current one, which corresponds to 11 August 3114 BC in the proleptic Gregorian calendar. This means that the fourth world reached the end of its 13th bʼakʼtun, or Maya date 13.0.0.0.0, on 21 December 2012. In 1957, Mayanist and astronomer Maud Worcester Makemson wrote that \"the completion of a Great Period of 13 bʼakʼtuns would have been of the utmost significance to the Maya.\" In 1966, Michael D. Coe wrote in The Maya that \"there is a suggestion ... that Armageddon would overtake the degenerate peoples of the world and all creation on the final day of the 13th [bʼakʼtun]. Thus ... our present universe [would] be annihilated ... when the Great Cycle of the Long Count reaches completion.', '2021-04-11 19:53:32', 8, '1fb0c959221da99c9a4d21a385b20ea0.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `doctrine_migration_versions`
--

CREATE TABLE `doctrine_migration_versions` (
  `version` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `executed_at` datetime DEFAULT NULL,
  `execution_time` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `doctrine_migration_versions`
--

INSERT INTO `doctrine_migration_versions` (`version`, `executed_at`, `execution_time`) VALUES
('DoctrineMigrations\\Version20210402094415', '2021-04-02 11:44:30', 53),
('DoctrineMigrations\\Version20210403065756', '2021-04-03 08:58:14', 44),
('DoctrineMigrations\\Version20210409103551', '2021-04-09 12:35:59', 312),
('DoctrineMigrations\\Version20210410123854', '2021-04-10 14:38:58', 144),
('DoctrineMigrations\\Version20210410130358', '2021-04-10 15:04:02', 145),
('DoctrineMigrations\\Version20210410131239', '2021-04-10 15:12:45', 41),
('DoctrineMigrations\\Version20210410141406', '2021-04-10 16:14:10', 39);

-- --------------------------------------------------------

--
-- Table structure for table `message`
--

CREATE TABLE `message` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `message`
--

INSERT INTO `message` (`id`, `user_id`, `content`, `date`) VALUES
(25, 12, 'Hello world', '2021-04-11 17:34:43'),
(27, 13, 'welp', '2021-04-11 17:44:01'),
(28, 14, 'Bonjour', '2021-04-11 17:48:59'),
(29, 15, 'attrape moi si tu peux ', '2021-04-11 17:51:12'),
(30, 16, 'My disappointment is immeasurable and my day is ruined.', '2021-04-11 17:55:02'),
(31, 18, 'Je te bénie Thomas prophète d\'Alkas', '2021-04-12 08:53:42'),
(32, 19, 'I was there 3000 years ago ', '2021-04-12 08:57:33');

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE `product` (
  `id` int(11) NOT NULL,
  `author_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` int(11) NOT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `picture` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`id`, `author_id`, `name`, `price`, `description`, `picture`) VALUES
(7, 9, 'r-301', 400, 'OP gun ', '9a4435532412f47e24d35dac22c12e6d.jpg'),
(8, 9, 'shovel', 50, 'Trying to connect the dots, don\'t know what to tell my boss. Before you met me I was alright but things were kinda heavy. You just gotta ignite the light and let it shine. Glitter all over the room pink flamingos in the pool.', 'a79ee7df884b579efd1137f9592e9ca3.jpg'),
(9, 9, 'knife', 89, 'Trying to connect the dots, don\'t know what to tell my boss. Before you met me I was alright but things were kinda heavy. You just gotta ignite the light and let it shine. Glitter all over the room pink flamingos in the pool.', '1189159cac4b2ce5b9700c1645678d2e.jpg'),
(10, 9, 'machete', 280, 'Trying to connect the dots, don\'t know what to tell my boss. Before you met me I was alright but things were kinda heavy. You just gotta ignite the light and let it shine. Glitter all over the room pink flamingos in the pool.', '327e7018043aa0d83720c728b6ef658a.jpg'),
(11, 8, 'Golden toilet paper', 1000, 'Magically soft and cut from extracts of fine gold leaf, Claptone is proud to announce the launch of his all-new golden toilet paper.\r\nAvailable exclusively through CLAPTONE.com, the Claptone Golden Toilet Paper is an artisanal blend of premium cotton and gold leaf of the highest grade, handmade, designed and packaged locally in Berlin. Constructed of four layers of golden goodness, it is both velvety-soft to the touch and environmentally friendly.', '2c41caa78db8db858f54023e2fdb863b.jpg'),
(12, 8, 'Cigarette', 500, 'By the pound, \r\nSmoking kills :)', '8716e78d127c556f2d23e459026db5e2.jpg'),
(13, 8, 'Water', 300, 'Cristalline, c\'est mon eau', 'd596e73fcc27b2b76f3805ba279d48e6.jpg'),
(14, 8, '\"Herbe de provence\"', 2000, 'Cultivé en Corse, totalement bio', '68a4f3dcf33508ef3f9a6945be4abf9e.jpg'),
(15, 8, 'Proper Twelve', 1499, 'I drink to forget but I keep remembering', '7cd6c39e7279d9e80f642d8edd7a183e.jpg'),
(16, 8, 'Unleaded 98', 3600, '30L of fuel \r\ntotally not empty', 'b0af37b2518e9a94e0313d3d863e2a59.jpg'),
(18, 8, 'Pasta', 399, 'Pasta la vista', '3194cf24349963abd0fc40485fea4348.jpg'),
(19, 8, 'AK47', 4970, 'ratata', 'e75c991c8ee3413834a9c154c9e31a1b.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `username` varchar(180) COLLATE utf8mb4_unicode_ci NOT NULL,
  `roles` longtext COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '(DC2Type:json)',
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `profil_picture` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `username`, `roles`, `password`, `profil_picture`) VALUES
(8, 'test', '[]', '$argon2id$v=19$m=65536,t=4,p=1$bFJEb1d6aWdCdGpkT2VReA$b3LzMZodfrsQY2xtbYIne8sAmMOtfJhcV10TOkSNsuU', 'default.png.jpg'),
(9, 'tets', '[]', '$argon2id$v=19$m=65536,t=4,p=1$WDU4Y2p6UWxmcXhVVXJhbw$seiwQaFXhCrqTO+CODfa2yrvw/mBRo52cF4/6NP1wSI', 'f303ad2176a1ed8986b8b84a68abe549.jpg'),
(10, 'test2', '[]', '$argon2id$v=19$m=65536,t=4,p=1$VENMVFV1VnB4ak9hUWdhbQ$Y5COw8Vnf35BX5QUYAqmUZZiEcxsK23Nd8Komo/oMlA', 'default.png.jpg'),
(11, 'test3', '[]', '$argon2id$v=19$m=65536,t=4,p=1$QTFNTU11US9LZ1A5ZGxKRQ$vRu4ont7tSb1WCjI28GoTqrvabEMnfe2ormOI0wULuk', 'default.png.jpg'),
(12, 'admin', '[]', '$argon2id$v=19$m=65536,t=4,p=1$OEVVcHVpOWJYWWJaN3dlSQ$Kgjqjg6k8z1rwc4KliNhkqRYr2+oWRLszm1xeYP7GMk', 'debc79925d39e3f1908987bf4bb960b3.png'),
(13, 'Jean-michel', '[]', '$argon2id$v=19$m=65536,t=4,p=1$Y1VVSWFSaWQxUXNxWTUvVQ$OzTuvu8T8iWtZKxsVaoqAu+C32KFcFYAyU+Dr2056h4', 'default.png.jpg'),
(14, 'gertrude', '[]', '$argon2id$v=19$m=65536,t=4,p=1$aHRFOUFQMml6Lm10YmRleA$YIeX4u4Nldg/OYepfsT0vBD1nu2ipbdMg6CsHnPmYd8', 'default.png.jpg'),
(15, 'Poimu', '[]', '$argon2id$v=19$m=65536,t=4,p=1$S3VCQXJmM1NJNVlzbHdWTQ$6mT5p3+NOr6QMoNCQ1lVjL3NNuc0j3KJnx8CcQUjikM', '10ea058690b54ceea94723028a1d4aea.png'),
(16, 'thomas', '[]', '$argon2id$v=19$m=65536,t=4,p=1$eDQyL0FoMlA4OWtOcHZGWA$UJqmfmi4js1XzOhrz7XLsNJ2nXo1C8YVNElLRVtV85k', 'a6b92162eaf1e22f0bf18399c1960085.png'),
(18, 'Balkany', '[]', '$argon2id$v=19$m=65536,t=4,p=1$QWwvUHNWZ1ltRTRTclZiLg$ZHTQLPcrVT6CZXHFT1eyBCFXe4YIo7PQ0sEgcftIj1o', '2c24102015b3a729481c5e08e6ed07f6.jpg'),
(19, 'Da Queen', '[]', '$argon2id$v=19$m=65536,t=4,p=1$T2JNVUltWFZEbDFCbW9nUg$HEehm7bCtqnRLS34bsO1zI6WoRI+KyV6QAWc4Q65WfE', '63445f4ea98a6416d57d355710b87bb1.jpg');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `article`
--
ALTER TABLE `article`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_23A0E66F675F31B` (`author_id`);

--
-- Indexes for table `doctrine_migration_versions`
--
ALTER TABLE `doctrine_migration_versions`
  ADD PRIMARY KEY (`version`);

--
-- Indexes for table `message`
--
ALTER TABLE `message`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_B6BD307FA76ED395` (`user_id`);

--
-- Indexes for table `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_D34A04ADF675F31B` (`author_id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_8D93D649F85E0677` (`username`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `article`
--
ALTER TABLE `article`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `message`
--
ALTER TABLE `message`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;

--
-- AUTO_INCREMENT for table `product`
--
ALTER TABLE `product`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `article`
--
ALTER TABLE `article`
  ADD CONSTRAINT `FK_23A0E66F675F31B` FOREIGN KEY (`author_id`) REFERENCES `user` (`id`);

--
-- Constraints for table `message`
--
ALTER TABLE `message`
  ADD CONSTRAINT `FK_B6BD307FA76ED395` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`);

--
-- Constraints for table `product`
--
ALTER TABLE `product`
  ADD CONSTRAINT `FK_D34A04ADF675F31B` FOREIGN KEY (`author_id`) REFERENCES `user` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
