<?php

namespace App\Normalizer;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Serializer\Normalizer\ContextAwareDenormalizerInterface;

class EntityDenormalizer implements ContextAwareDenormalizerInterface {

    private $em;
    public function __construct(EntityManagerInterface $em){
        
        $this->em = $em;
    }

    public function denormalize($data, string $type, ?string $format = null, array $context = [])
    {
        return $this->em->find($type, $data);
    }

    public function supportsDenormalization($data, string $type, ?string $format = null, array $context = [])
    {
        return (strpos($type, 'App\\Entity\\') === 0) && (is_numeric($data) );
    }
}
