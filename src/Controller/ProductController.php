<?php

namespace App\Controller;

use App\Entity\Product;
use App\Form\ProductType;
use App\Repository\ProductRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/product')]
class ProductController extends AbstractController
{
    #[Route('', name: 'product', methods: ['GET'])]
    public function index(ProductRepository $productRepository): Response
    {
        $products = $productRepository->findAll();
        return $this->render('product/index.html.twig', [
            "products" => $products,
        ]);
    }

    #[Route('/view/{id}', name: 'view', methods: ['GET'])]
    public function view(int $id, ProductRepository $productRepository): Response
    {
        
        $product = $productRepository->find($id);
        return $this->render
        (
            'product/view.html.twig',
            [
                "product" => $product
            ]
        );
    }

    #[Route('/new', name: 'product_create', methods: ['GET', 'POST'])]
    public function new(Request $request): Response
    {
        $product = new Product();
        $form = $this->createForm(ProductType::class, $product);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $product->setAuthor($this->getUser());
            $productPicture = $form->get('productPicture')->getData();
            if ($productPicture) {
                $pictureName = md5(uniqid(rand())) . "." . $productPicture->guessExtension();
                $pictureDestination = $this->getParameter('product_pictures_dir');
                try {
                    $productPicture->move($pictureDestination, $pictureName);
                } catch (FileException $e) {
                    throw new HttpException(500, 'an error occured during file upload');
                }
                $product->setPicture($pictureName);
            }

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($product);
            $entityManager->flush();

            return $this->redirectToRoute('product');
        }

        return $this->render('product/new.html.twig', [
            'product' => $product,
            'form' => $form->createView(),
        ]);
    }

    #[Route('/delete/{id}', name: 'product_delete')]
    public function delete(Product $product): Response
    {
        $user = $this->getUser();
        if ($user->getId() == $product->getAuthor()->getId()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($product);
            $entityManager->flush();
        }
        
        

        return $this->redirectToRoute('product');
    }
    
}
