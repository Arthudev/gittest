<?php

namespace App\Controller;

use App\Entity\Message;
use App\Repository\MessageRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/message')]
class MessageController extends AbstractController
{
    #[Route('', name: 'message', methods: ["GET", "HEAD"])]
    public function list(MessageRepository $messageRepository): Response
    {
        $messages = $messageRepository->findAll();
        return $this->render('message/index.html.twig', [
            "messages" => $messages,
        ]);
    }

    #[Route('/create', name: 'message_create', methods: ["POST"])]
    public function create(Request $request)
    {
        $this->denyAccessUnlessGranted('ROLE_USER');
        $content = $request->request->get('chat-content');
        if (!$content) {
            return $this->redirectToRoute("message");
        }
        $message = new Message();
        $message->setContent($content);
        $message->setUser($this->getUser());
        $message->setDate(new \DateTime());
        $em = $this->getDoctrine()->getManager();
        $em->persist($message);
        $em->flush();
        return $this->redirectToRoute("message");


    }
}
